# Experimental Ostree Native Container images for rpm-ostree based Fedora desktop variants

## Overview

This repo is a fork of
[pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config)
with CI and minor changes on top to enable us to build experiemental Ostree
Native Container images for rpm-ostree based Fedora desktop variants.

The offcial upstream sources for Fedora Silverblue, Fedora Kinoite, Fedora
Sericea and Fedora Onyx remain at
[pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config)
and the official builds are only available from the Fedora ostree repo for now.

See [Switch to container native ostree format by default](https://gitlab.com/fedora/ostree/sig/-/issues/2)
for the work in progress to build official Ostree Native Container images in the Fedora infrastructure.

## Issues and PRs

Please submit PRs at
[pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config)
and file issues in the respective projects issue trackers:

- For issues impacting all Atomic desktop variants: [Fedora Atomic Desktops issue tracker](https://gitlab.com/fedora/ostree/sig/-/issues)
- For Fedora Silverblue specific issues: [github.com/fedora-silverblue/issue-tracker](https://github.com/fedora-silverblue/issue-tracker/issues)
- For Fedora Kinoite specific issues: [pagure.io/fedora-kde/SIG](https://pagure.io/fedora-kde/SIG/issues)
- For Fedora Sericea specific issues: [gitlab.com/fedora/sigs/sway/SIG](https://gitlab.com/fedora/sigs/sway/SIG/-/issues)
- For Fedora Onyx specific issues: [pagure.io/fedora-budgie](https://pagure.io/fedora-budgie/project/issues)

## Images built

This project builds the following images for all Fedora releases:

- Fedora Silverblue:
    - Unofficial build based on the official Silverblue variant
    - [quay.io/repository/fedora-ostree-desktops/silverblue](https://quay.io/repository/fedora-ostree-desktops/silverblue?tab=tags)
- Fedora Kinoite:
    - Unofficial build based on the official Kinoite variant
    - [quay.io/repository/fedora-ostree-desktops/kinoite](https://quay.io/repository/fedora-ostree-desktops/kinoite?tab=tags)
- Fedora Sericea:
    - Unofficial build based on the official Sericea variant
    - [quay.io/repository/fedora-ostree-desktops/sericea](https://quay.io/repository/fedora-ostree-desktops/sericea?tab=tags)
- Fedora Onyx:
    - Unofficial build based on the official Onyx variant
    - [quay.io/repository/fedora-ostree-desktops/onyx](https://quay.io/repository/fedora-ostree-desktops/onyx?tab=tags)
    - Only since Fedora 39
- Fedora Lazurite:
    - Unofficial LXQt variant
    - [quay.io/repository/fedora-ostree-desktops/lazurite](https://quay.io/repository/fedora-ostree-desktops/lazurite?tab=tags)
    - Only since Fedora 39
- Fedora Vauxite:
    - Unofficial XFCE variant
    - [quay.io/repository/fedora-ostree-desktops/vauxite](https://quay.io/repository/fedora-ostree-desktops/vauxite?tab=tags)
- Fedora Base:
    - Minimal image with no desktop environment
    - [quay.io/repository/fedora-ostree-desktops/base](https://quay.io/repository/fedora-ostree-desktops/base?tab=tags)

Special images that may not always be available,
(see [Introducing Kinoite Nightly (and Kinoite Beta)](https://tim.siosm.fr/blog/2023/01/20/introducing-kinoite-nightly-beta/)):

- Fedora Kinoite Beta:
    - Unofficial Kinoite variant with KDE Plasma Beta packages from [@kdesig/kde-beta](https://copr.fedorainfracloud.org/coprs/g/kdesig/kde-beta/)
    - [quay.io/repository/fedora-ostree-desktops/kinoite-beta](https://quay.io/repository/fedora-ostree-desktops/kinoite-beta?tab=tags)
- Fedora Kinoite Nightly:
    - Unofficial Kinoite variant with nightly KDE packages from [@kdesig/kde-nightly](https://copr.fedorainfracloud.org/coprs/g/kdesig/kde-nightly/packages/)
    - [quay.io/repository/fedora-ostree-desktops/kinoite-nightly](https://quay.io/repository/fedora-ostree-desktops/kinoite-nightly?tab=tags)

## Can I add an image here? How do I add my image?

In this repo, we will only build images from official Fedora RPM packages or
from COPR repos maintained by official Fedora SIGs.

File an issue in this repo if you want another desktop variant to be built.

In all other cases, you will have to host your own CI and images. Take a look
at [github.com/ublue-os/base](https://github.com/ublue-os/base) for examples.

If you want to maintain a new official image in Fedora, you can follow the
[How to make a new rpm-ostree desktop variant in Fedora?](https://tim.siosm.fr/blog/2023/06/21/rpm-ostree-variants-fedora/)
guide.
